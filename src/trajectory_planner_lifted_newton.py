#!/usr/bin/env python

#/*****************************************************************************
# *                                                                           *
# *   trajectory_planner_lifted_newton.py                                             *
# *                                                                           *
# *   ROS                                                                     *
# *                                                                           *
# *   Copyright (C) 2015 by Lukasz Chojnacki                                  *
# *   lukasz.chojnacki@pwr.edu.pl                                               *
# *                                                                           *
# *   This program is free software; you can redistribute it and/or modify    *
# *   it under the terms of the GNU General Public License as published by    *
# *   the Free Software Foundation; either version 2 of the License, or       *
# *   (at your option) any later version.                                     *
# *                                                                           *
# *   This program is distributed in the hope that it will be useful,         *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
# *   GNU General Public License for more details.                            *
# *                                                                           *
# *   You should have received a copy of the GNU General Public License       *
# *   along with this program; if not, write to the                           *
# *   Free Software Foundation, Inc.,                                         *
# *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
# *                                                                           *
# *****************************************************************************/

from sys import path
path.append(r"/home/lukas/catkin_ws/src/motion_planning_and_tracking_pkg/src/ECSA/casadi-py27-np1.9.1-v3.0.0")
from casadi import *

import rospy

from robrex.msg import ack, desired_plan, ready, traj

from ECSA.Control import Fourier
from ECSA.System_base import Monocycle
from ECSA.Algorithm import LiftedNewton


readyFlag = False
ackFlag = True


#-------------------------------------------#
# Algorithm parameters

# derivative time
T = int(rospy.get_param('/robrex/algorithm/T',10.0))#s
rospy.set_param('/robrex/algorithm/T',T)
#T = T * 1000#ms

# algoritm step
gamma = float(rospy.get_param('/robrex/algorithm/gamma',0.94))
rospy.set_param('/robrex/algorithm/gamma',gamma)

# total tolerance error
TOL_tmp = rospy.get_param('/robrex/algorithm/TOL',[1,10,-3])
TOL = 10**(-3)#float(TOL_tmp[0])*pow(float(TOL_tmp[1]),float(TOL_tmp[2]))
rospy.set_param('/robrex/algorithm/TOL',TOL)

# maximum number of steps
k_max = int(rospy.get_param('/robrex/algorithm/k_max',300) )
rospy.set_param('/robrex/algorithm/k_max',k_max)

# 
dts = rospy.get_param('/robrex/controller/dts',0.04)#s
rospy.set_param('/robrex/controller/dts',dts)

#
Ts = rospy.get_param('/robrex/controller/Ts',1.0)#s
rospy.set_param('/robrex/controller/Ts',Ts)

d_notify = int(rospy.get_param('/robrex/controller/d_notify',1))
rospy.set_param('/robrex/controller/d_notify',d_notify)

step = Ts/dts

notify = step - d_notify

#-------------------------------------------#
opts = {}
opts['T'] = T
opts['gamma0'] = gamma
opts['TOL'] = TOL
opts['k_max'] = k_max


la1 = [0.5, 0.25, -0.25, 0.1, 0.1]
la2 = [-0.5, 0.25, 0.25, 0.1, 0.1, 0.01, 0.01]
opts['lambda0'] = vertcat([0.5, 0.25, -0.25, 0.1, 0.1],[ -0.5, 0.25, 0.25, 0.1, 0.1, 0.01, 0.01])
opts['lambda'] = [[len(la1),0],[len(la2),0]]

opts['viz'] = [False ,False,0.2]


fourier = Fourier(opts)
system = Monocycle(fourier)


def LiftedNewtonAlgorithm(Yd):

    ## translate point list
    x0 = [0 for x in range(system._dim['n'])]
    for i in range(len(Yd[0])):
        x0[i] = Yd[0][i]

    Yd.pop(0)
    opts['q0'] = x0
    opts['x0'] = []
    opts['yd'] = Yd

    # create algorithm object
    algorithm = LiftedNewton(system,opts)

    # run algorithm
    out = algorithm.run()


    q1 = algorithm.CasadiToNumpy(out['q'][0,:].T)
    q2 = algorithm.CasadiToNumpy(out['q'][1,:].T)
    q3 = algorithm.CasadiToNumpy(out['q'][2,:].T)


    # return trajectory (q1,q2,q3)
    # return algo.getTrajectory(Ts,dts)
    return q1,q2,q3,1

def DesiredPlan(data):
    global ackFlag
    global readyFlag
    global q1,q2,q3,trajParts
    global m


    if ackFlag == True:

        m = 0

        # read data from topic /desired_plan
        # build a point map form a recived vector
        Yd = PointListFromVector(data.desired_plan)
        print "Yd=",Yd

        ackFlag = not ackFlag

        # generate trajectory
        q1,q2,q3,trajParts = LiftedNewtonAlgorithm(Yd)

        trajmsg = traj()

        # fill messafe
        # trajmsg.q1 = q1[m]
        # trajmsg.q2 = q2[m]
        # trajmsg.q3 = q3[m]
        trajmsg.q1 = q1
        trajmsg.q2 = q2
        trajmsg.q3 = q3
        trajmsg.step = step
        trajmsg.notify = notify

                # publish message at topic /traj
        pubTraj.publish(trajmsg)
        trajParts = trajParts - 1
        m = m + 1

    else:
        rospy.loginfo("Sorry, I'm working. ack flag is false. ")


def Ready(data):
    global ackFlag
    global trajParts,m

    # read data from topic /ready
    readyFlag = data.ready

#    print readyFlag,trajParts,m


    if readyFlag > 0 and trajParts > 0:

        readyFlag = False
        # create message
        trajmsg = traj()

        # fill message
        trajmsg.q1 = q1
        trajmsg.q2 = q2
        trajmsg.q3 = q3
        trajmsg.step = step
        trajmsg.notify = notify

        # publish message at topic /traj
        pubTraj.publish(trajmsg)
        trajParts = trajParts - 1
        m = m + 1

    if readyFlag > 0 or not trajParts:
        ackFlag = True
    else:
        ackFlag = False

    # create message of type ack
    ackmsg = ack()

    # fill message
    ackmsg.ack = ackFlag
        
    # publish message at topic /ack
    pubAck.publish(ackmsg)

    #print(readyFlag,trajParts)

def node():
    global pubAck
    global pubTraj

    # Init ROS Node /LiftedNewtonAlgorithm
    rospy.init_node('trajectory_planner_lifted_newton')

    # Publishers
    pubAck = rospy.Publisher("ack", ack, queue_size=100)
    pubTraj = rospy.Publisher("traj", traj, queue_size=100)
    
    # Subscribers
    rospy.Subscriber("desired_plan", desired_plan, DesiredPlan)
    rospy.Subscriber("ready", ready, Ready)
    
    #print T,gamma,TOL,k_max,dts,Ts
    
    rospy.spin()


def PointListFromVector(vector):
    Yd = []

    for m in range(0,len(vector)-system._dim['n'],2):#zmienic zalezne od modelu
        yi = []
        for n in range(2):
            yi.append(vector[m+n])
        Yd.append(yi)
    yi = []

    for m in range(len(vector)-system._dim['n'],len(vector)):
        yi.append(vector[m])
    Yd.append(yi)  
    return Yd  

if __name__ == '__main__':
    try:
        node()
    except rospy.ROSInterruptException:
        pass






