from sys import path
path.append(r"../casadi-py27-np1.9.1-v3.0.0")
from casadi import *

import numpy

from ECSA.API.ECSASingleShooting import ECSASingleShooting


class Extended(ECSASingleShooting):
    def __init__(self,system,opts):
        ECSASingleShooting.__init__(self, system, opts)


    def init(self,opts):
        integrator_opts = { 't0':0,
                            'tf':opts['T'],
                            "abstol":1e-6,
                            "reltol":1e-6
        #                   "cvodes.solver",'ode45'
                        }

        ## system for integrator
        system = {'t':self._system._t,
                  'x':self._system._q,
                  'p':self._system._control._lambda,
                  'ode':self._system.dq(self._system._q,
                                        self._system._u
                                        )
                  }



        # CasADi integrator
        Im = integrator("Im", "cvodes", system, integrator_opts)
        ode = Im.jacobian("p","xf")

        ## extention
        # H = opts['H']
        bounds = opts['bounds']

        u = []
        lb = []
        ub = []
        alpha = []

        for key in bounds.keys():
            u.append(self._system._u[int(key[1])])
            lb.append(bounds[key][0])
            ub.append(bounds[key][1])
            alpha.append(bounds[key][2])


        if len(u) < 1:
            print "Define bounds for the Extended algorithm"
            self._out['fault'] = 1
            return 1

        h = self.dh(u[0],lb[0],ub[0],alpha[0])

        extention = {'t':self._system._t,
                     'x':SX.sym("u",1),
                     'p':self._system._control._lambda,
                     'ode':h
        }

        Ie = integrator("Ie","cvodes",extention,integrator_opts)
        odeE = Ie.jacobian("p","xf")




        # algorithm initial conditions
        ##initial condition
        param = {}
        param['error'] = [100,100]
        param['system'] = self._system
        param['ode'] = [ode,odeE]

        return param


    def getKinematicsAndJacobian(self,param,opts):
        sol = param['ode'][0](x0=opts['q0'],p=param['lambda'])
        param['q'] = sol["xf"]
        param['x'] = opts['x0']
        Jt = sol["dxf_dp"]



        param['J'] = numpy.dot(self._system.C(param['q'],param['x']), Jt)# + numpy.dot(system_base.D(q,x),x0)

        #----------------------------------------------------

        param['H'] = self.getH(param,opts)
        param['DH'] = self.getDH(param,opts)

        J = vertcat(param['J'],param['DH'])
        q = vertcat(param['q'],param['H'])
        x = param['x']

        return q,x,J



    def getInverseJacobian(self,param,opts):
        return [numpy.linalg.inv(param['J'])]

    def getError(self,param,opts):
        err = self._system.y(param['q'],param['x']) - opts['yd']
        return [vertcat(err,param['H'])]

    def getGamma(self,param,opts):
        dLam = mtimes(param['Jp'][0],param['error'][0])
        gmm = 2*opts['gamma0']
        nrm = norm_2(dLam)
        return [min(sqrt(gmm/nrm),1)]

    def getEndogenousConfiguration(self,param,opts):
        lambda0 =  param['lambda'] - param['gamma'][0]*mtimes(param['Jp'][0],param['error'][0])
        return lambda0,opts['x0']

    def getH(self,param,opts):
        sol = param['ode'][1](x0=0,p=param['lambda'])
        DF = sol["dxf_dp"]
        K = self.null(param['J'])
        return mtimes(K.T,DF.T)

    def getDH(self,param,opts):

        DH = []

        delta_la = 0.001
        for i in range(self._system._control._dim['s']):
            dla = numpy.zeros((self._system._control._dim['s'],1))
            dla[i] = delta_la
            dJ = param['ode'][0](x0=opts['q0'],p=param['lambda']+dla)['dxf_dp']
            dJ = numpy.dot(self._system.C(param['q'],param['x']),dJ)
            dJf = param['ode'][1](x0=0,p=param['lambda']+dla)['dxf_dp']
            DH = horzcat(DH,(mtimes(self.null(dJ).T,dJf.T)-param['H'])/delta_la)

        return DH

    def null(self,a, rtol=1e-5):
        u, s, v = numpy.linalg.svd(a)
        rank = (s > rtol*s[0]).sum()
        return v[rank:].T.copy()


    def CasadiToNumpy(self,ktmp):
        k1 = numpy.zeros((numpy.shape(ktmp)[0],numpy.shape(ktmp)[1]))
        for j in range(0,numpy.shape(ktmp)[0]):#zamiana z typu casadi na liste
            for i in range(0,numpy.shape(ktmp)[1]):
                k1[j,i] = ktmp[j,i]
        return k1

    def CasadiToNumpyVect(self,ktmp):
        k1 = numpy.zeros(numpy.shape(ktmp)[0])
        for j in range(numpy.shape(ktmp)[0]):#zamiana z typu casadi na liste
            k1[j] = ktmp[j]
        return k1


    def r(self,u):
        return atan(u)**2


        ## Funkcja g - funkcja ograniczajaca
        # @param self -- wskaznik na obiekt
        # @param x -- q lub u w zaleznosci od sytuacji
        # @return p(x-ub)+p(-x+lb)
    def dh(self,u,lb,ub,alpha):
        # h = 0
        # for i in range(u.size1()):
        #     h = h + p(u[i]-ub,alpha)+p(-u[i]+lb,alpha)

        return self.p(u-ub,alpha)+self.p(-u+lb,alpha)+self.r(u)

        ## Funkcja Mangarasian p(x,alfa)
        # @param self -- wskaznik na obiekt
        # @param x -- argument funkcji
        # @return wartosc funkcji w punkcie x
    def p(self,x,alpha):
            # print alpha
            # print x.is_constant()
        if x.is_constant() and -alpha*x > 1e+02:
            return 0
        else:
            return x + 1/alpha*numpy.log(1 + numpy.exp(-alpha*x))