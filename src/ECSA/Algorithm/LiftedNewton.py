from sys import path
path.append(r"../casadi-py27-np1.9.1-v3.0.0")
from casadi import *

from multiprocessing import Process,Queue

output = Queue()

from ECSA.API.ECSAMultiShooting import ECSAMultiShooting

class LiftedNewton(ECSAMultiShooting):
    def __init__(self,system,opts):
        ECSAMultiShooting.__init__(self, system, opts)


    def CasadiToNumpy(self,ktmp):
        k1 = numpy.zeros(numpy.shape(ktmp)[0])
        for j in range(numpy.shape(ktmp)[0]):#zamiana z typu casadi na liste
            k1[j] = ktmp[j]
        return k1

    ## wstawia mala macierz we wskazane pola duzej macierzy
    #    @pre cell i matrix musza byc wektorami
    #    @param row -- wiersz wstawienia
    #    @param column -- kolumna wstawienia
    #    @param cell -- wstawiana macierz
    #    @param cellRow -- ilosc wierszy wstawianej macierzy
    #    @param cellCol -- ilosc kolumn wstawianej macierzy
    #    @param matrix -- macierz do ktorej jest wstawiana wartosc
    #    @param matRow -- ilosc wierzy macierzy
    #    @param matCol -- ilsoc kolumn macierzy
    def setMatrixToCell(self,row, column, cell, cellRow, cellCol, matrix, matRow, matCol):
        colm=0
        ro=-1
        for v in range(len(cell)):
            if(v%cellCol==0):
                ro+=1
                colm=0
            else:
                colm+=1
            y = column*cellCol
            x = (((row*cellRow)+ro)*(matCol))+colm
            matrix[x+y] = cell[v]

    ## Obliczenie rownan rozniczkowych pomiedzy punktami
    #    @param self -- wskaznik na obiekt
    #    @param param --
    def fromPointToPoint(self,m,param,dim):
        opts = {}
        opts["t0"] = param['Tn']*(m)
        opts["tf"] = param['Tn']*(m+1)
        q0 = param['s'][m]
        lambda0 = param['lambda'][m]

        ## system for integrator
        system = {'t':self._system._t,
                  'x':self._system._q,
                  'p':self._system._control._lambda,
                  'ode':self._system.dq(self._system._q,
                                        self._system._u
                                        )
                  }

        ## integrator
        Im = integrator("Im", "cvodes", system, opts)
        jacobi = Im.jacobian("p","xf")
        sol = jacobi(x0=q0,p=lambda0)
        q = np.array(sol["xf"])
        vJ = np.array(sol["dxf_dp"]).flatten()

        dintegrator = Im.derivative(1,0)
        for i in range(dim['n']):
            fwd0_x0 = numpy.zeros(dim['n'])
            fwd0_x0[i] = 1
            dhds = numpy.array(dintegrator(der_x0=q0,der_p=lambda0,fwd0_x0=fwd0_x0)['fwd0_xf']).transpose()[0]
            if i == 0:
                G = dhds
            else:
                G = numpy.hstack((G,dhds))

        G = G.reshape((dim['n'],dim['n']))
        vG = np.array(G).transpose().flatten()

        H = np.array(q).reshape(1,dim['n'])
        G = H - param['s'][m+1]

        retval = [m,q,H,G,vJ,vG]

        output.put(retval)

        return retval

    def init(self,opts):
        N = len(opts['yd'])

        lambd = opts['lambda0']
        for i in range(1,N):
            lambd = numpy.hstack((lambd,opts['lambda0']))
        lambd = lambd.T

        Tn = opts['T']/N #czas pomiedzy pkt posrednimi

        ##macierzy H
        # wymiar N x n
        H = numpy.zeros((N,self._system._dim['n']))
        G = numpy.ones((N+1,self._system._dim['n']))*100

        ##macierz dHds(s,lambda)
        # wymiar nN x nN
        dHds = numpy.zeros(self._system._dim['n']*N*self._system._dim['n']*N)

        ##macierz dHdlambda(s,lambda) -- wersja wektorowa
        # wymiar nN x mMN
        vdHdlambd = numpy.zeros(N*self._system._dim['n']*N*self._system._control._dim['s'])

        r = 0
        for m in range(numpy.shape(opts['yd'])[0]):
            r = r + numpy.shape(opts['yd'][m])[0]

        s = numpy.zeros((N,self._system._dim['n']))
        for m in range(0,len(opts['yd'])):
            for n in range(0,len(opts['yd'][m])):
                s[m][n]  = opts['yd'][m][n]

        param = {}
        param['k_vect'] = []
        param['step'] = 0
        # param['error'] = [100]
        param['system'] = self._system
        param['lambda'] = lambd
        # param['ode'] = ode
        # param['system'] = system


        param['Tn'] = Tn
        param['N'] = N

        param['H'] = H
        param['G'] = G
        param['dHds'] = dHds
        param['vdHdlambda'] = vdHdlambd
        param['r'] = r
        param['s'] = s
        return param

    def getJacobianAndError(self,param,opts):
        param['e'] = []
        param['deds'] = numpy.zeros(param['r']*param['N']*self._system._dim['n'])
        param['s'] = numpy.vstack((opts['q0'],param['s']))


        processes = [Process(target=self.fromPointToPoint,args=(x,
                                                                param,
                                                                self._system._dim
                                                                )
                             )
                     for x in range(param['N'])
                     ]

        # Run processes
        for p in processes:
            p.start()


        results = [output.get() for p in processes]
        results.sort()

        # Exit the completed processes
        for p in processes:
            p.join()



        for m in range(param['N']):
            param['H'][m] = results[m][2]
            param['G'][m] = results[m][3]
            ## wektor jakobianu z Q
            vJ = results[m][4]
            ## wstawienie macierzy J do macierzy dHdlambda
            self.setMatrixToCell(m,
                                 m,
                                 vJ,
                                 self._system._dim['n'],
                                 self._system._control._dim['s'],
                                 param['vdHdlambda'],
                                 param['N']*self._system._dim['n'],
                                 param['N']*self._system._control._dim['s']
                                 )
            if m>0:
                ## wektor Gamma z Q
                vG = results[m][5]
                ## wstawienie macierzy Gamma do macierzy dHds
                self.setMatrixToCell(m,
                                     m-1,
                                     vG,
                                     self._system._dim['n'],
                                     self._system._dim['n'],
                                     param['dHds'],
                                     self._system._dim['n']*param['N'],
                                     self._system._dim['n']*param['N']
                                     )
            if m != param['N']-1:
                ## Calculate tast space error (err)
                param['e'].extend(self.CasadiToNumpy(self._system.yi(param['s'][m+1],0)) - opts['yd'][m])
                ## Calculate respective derivative (deds)
                #  deds -- wymiar r x nN
                C = numpy.array(self._system.Ci(param['H'][m],0))
                tmpRow = numpy.shape(C)[0]
                tmpCol = numpy.shape(C)[1]
                C = C.flatten()
                self.setMatrixToCell(m,
                                     m,
                                     C,
                                     tmpRow,
                                     tmpCol,
                                     param['deds'],
                                     param['r'],
                                     param['N']*self._system._dim['n']
                                     )
            else:
                ## Calculate tast space error (err)
                #  obliczenie bledu
                param['e'].extend(self.CasadiToNumpy(self._system.y(opts['yd'][m],0)) - opts['yd'][m])


                ## Calculate respective derivative (deds)
                #  deds -- wymiar r x nN
                C = numpy.array(self._system.C(param['H'][m],0))
                # tmpRow = numpy.shape(C)[0]
                tmpCol = numpy.shape(C)[1]
                C = C.flatten()
                self.setMatrixToCell(m,
                                     m,
                                     C,
                                     tmpRow ,
                                     tmpCol,
                                     param['deds'],
                                     param['r'],
                                     param['N']*self._system._dim['n']
                                     )

        k = self.CasadiToNumpy(self._system.y(param['s'][-1],0))
        for m in range(0,len(param['G'][param['N']])):
            param['G'][param['N'],m] = (k - opts['yd'][-1])[m]

        ## macierz pkt posrednich
        #  wykorzystywana do wyrysowania pkt posrednich
        for m in range(self._system._dim['n']):
            param['s'] = numpy.delete(param['s'], 0)
        param['s'] = param['s'].reshape(param['N'], self._system._dim['n'])



        param['dHdlambda'] = param['vdHdlambda'].reshape(param['N']*self._system._dim['n'],
                                                       param['N']*self._system._control._dim['s'])
        param['deds'] = param['deds'].reshape(param['r'], param['N']*self._system._dim['n'])
        param['dHds'] = param['dHds'].reshape(self._system._dim['n']*param['N'],self._system._dim['n']*param['N'])


        return param

    def getHelpVariable(self,param,opts):
        ## Set helper variable
        # M  -- wymiar nN x nN
        M = -np.linalg.inv(np.array(param['dHds']-numpy.eye(self._system._dim['n']*param['N'])))
        param['dHds'] = param['dHds'].flatten()



        ## Calculate z,Z,w,W
        # z -- wymiar nN x 1
        # Z -- wymiar nN x mMN
        # w -- wymiar n x 1
        # W -- wymiar n x mMN
        Hs = param['H'] -param['s']
        Hs = Hs.flatten()

        param['z'] = numpy.dot(M,Hs)
        param['Z'] = numpy.dot(M,param['dHdlambda'])
        param['w'] = param['e'] + numpy.dot(param['deds'],param['z'])
        param['W'] = numpy.dot(param['deds'],param['Z'])

        return param

    def getNewtonStepLength(self,param,opts):
        ## Calculate Newton step lenght
        # Wpinv -- wymiar mMN x n
        # dlambda -- wymiar mMN x 1
        # ds -- wymiar nN x 1
        dlam = - numpy.dot(pinv(param['W']),param['w'])
        return [dlam,
                param['z'] + numpy.dot(param['Z'],dlam)
                ]

    def getGamma(self,param,opts):
        ## Calculate damping factors
        gmm = 2*opts['gamma0']
        return [float(min(sqrt(gmm/norm_2(param['dlambda'][:,None])),1)),
                float(min(sqrt(gmm/norm_2(param['ds'])),1))
                ]

    def getNewtonStep(self,param,opts):
        # Perform Newton step
        # lambd -- wymiar mMN
        # s -- wymiar nN
        param['lambda'] = param['lambda'].flatten()
        param['s'] = param['s'].flatten()

        param['lambda'] = param['lambda'] + param['gamma'][0] * param['dlambda']
        param['s'] = param['s'] + param['gamma'][1] * param['ds']

        param['lambda'] = param['lambda'].reshape(param['N'],self._system._control._dim['s'])
        param['s'] = param['s'].reshape(param['N'],self._system._dim['n'])

        return param['lambda'],param['s']