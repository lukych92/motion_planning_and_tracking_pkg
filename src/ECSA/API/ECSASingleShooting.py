from sys import path
path.append(r"../casadi-py27-np1.9.1-v3.0.0")
from casadi import *

from ECSA.API import ECSAAlgorithm
import ECSA.API.Visualization


## The class model ECSA Single Shooting group of algorithms
class ECSASingleShooting(ECSAAlgorithm):
    ## The initialization method
    #
    # @param self the object pointer
    # @param system the object of the class System or System_base
    # @param opts a dictionary of algorithm options
    def __init__(self,system,opts):
        ECSAAlgorithm.__init__(self, system, opts)

    ## Function check initial data
    #
    # @retval 1 everything is OK
    # @retval 0 something wrong
    def checkData(self):
        if self._system._dim['n'] != len(self._opts['q0']):
            print "The dimension of q0 is different that the dimension n"
            self._out['fault'] = 1
            return 0
        elif self._system._dim['p'] != len(self._opts['x0']):
            print "The dimension of x0 is different that the dimension p"
            self._out['fault'] = 1
            return 0
        elif self._system._dim['r'] != len(self._opts['yd']):
            print "The dimension of yd is different that the dimension r"
            self._out['fault'] = 1
            return 0
        elif self._system._dim['m'] != self._system._control._dim['m']:
            print "The dimension of controls is different that the dimension m"
            self._out['fault'] = 1
            return 0
        else:
            self._out['fault'] = 0
            return 1


    ## The function define kinematics and jacobian
    # @param self the object pointer
    # @param opts a dictionary of options for the algorithm
    # @param param a dictionary of the parameters
    # @retval q state value at end of the time horizon
    # @retval x  at end of the time horizon
    # @retval J Jacobian of the system
    def getKinematicsAndJacobian(self,param,opts):
        raise "Implement yor definition of kinematics and jacobian"

    ## The function define the inverse Jacobian
    # @param self the object pointer
    # @param opts a dictionary of options for the algorithm
    # @param param a dictionary of the parameters
    # @retval [invJ, ...] a list of inverse Jacobian
    def getInverseJacobian(self,param,opts):
        raise "Implement your definition of jacobian inverse"

    ## The function define the meaning of algorithm error
    # @param self the object pointer
    # @param opts a dictionary of options for the algorithm
    # @param param a dictionary of the parameters
    # @retval [error1, ...] a list of errors
    def getError(self,param,opts):
        raise "Implement your definition of error"

    ## The function define gamma parameter
    # @param self the object pointer
    # @param opts a dictionary of options for the algorithm
    # @param param a dictionary of the parameters
    # @retval [gamma1, ...] a list
    def getGamma(self,param,opts):
        raise "Implement your definition of gamma"

    ## The function define new endogenous configuration, a pair (lambda vector, joint position)
    # @param self the object pointer
    # @param opts a dictionary of options for the algorithm
    # @param param a dictionary of the parameters
    # @retval lambda,x new lambda vector and new joint position
    def getEndogenousConfiguration(self,param,opts):
        raise "Implement your definition of Endogenous Configuration"

    ## Function provide while loop for the ECSA
    #
    # @param self the object pointer
    # @param opts a dictionary of options for the algorithm
    # @param param a dictionary of the parameters
    # @retval param a dictionary of the final parameters
    def mainLoop(self,param,opts):
        param['step'] = 0
        param['lambda'] = self._opts['lambda0']
        while param['step'] < opts['k_max'] and numpy.linalg.norm(param['error'][0]) > opts['TOL']:
            param['q'],param['x'],param['J'] = self.getKinematicsAndJacobian(param,opts)
            param['Jp'] = self.getInverseJacobian(param,opts)
            param['error'] = self.getError(param,opts)
            param['gamma'] = self.getGamma(param,opts)
            param["lambda"],param['x'] = self.getEndogenousConfiguration(param,opts)
            param['step'] = param['step'] + 1

            self.updateErrorVector(param)
            self.visualizationOneStep(param,opts)

        return param

    ## Function visualize the desired parameters at each step
    #
    # @param self the object pointer
    # @param opts a dictionary of options for the algorithm
    # @param param a dictionary of the parameters
    def visualizationOneStep(self,param,opts):
        print "step: %d " % param['step']
        for task in range(len(param['error'])):
            if len(param['error']) > 1:
                print "%1s subtask %d" % ("",task)
            print "%4s error: %.5f" %("",numpy.linalg.norm(param['error'][task]))
            print "%4s det: %e" %("",numpy.linalg.det(mtimes(param['Jp'][task].T,param['Jp'][task])))
            print "%4s rank of Jacobi matrix: %d" %("",numpy.linalg.matrix_rank(param['Jp'][task]))
            print "%4s condition number: %.2f" %("",numpy.linalg.cond(param['Jp'][task]))

        if opts['viz'][0] == True:
            self.setOutput(param)
            ECSA.API.Visualization.showPath(self._out, self._opts,
                                            isBlocked=opts['viz'][1],
                                            pauseTime=opts['viz'][2])

    ## Function put actual error and put it into param['error_vect']
    # @param self the object pointer
    # @param param a dictionary of the parameters
    def updateErrorVector(self,param):
        if param['step'] == 1:
            param['error_vect'] = param['error']
        else:
            for i in range(len(param['error'])):
                param['error_vect'][i] = horzcat(param['error_vect'][i], param['error'][i])

    ## Function set number of algorithm steps into self._out[] dictionary
    # @param self the object pointer
    # @param param a dictionary of the parameters
    def setOutStep(self,param):
        self._out['step'] = param['step']

    ## Function set lambda vector into self._out[] dictionary
    # @param self the object pointer
    # @param param a dictionary of the parameters
    def setOutLambda(self,param):
        self._out['lambda'] = param['lambda']

    ## Function set time vector into self._out[] dictionary
    # @param self the object pointer
    # @param param a dictionary of the parameters
    def setOutTime(self,param):
        self._out['tgrid'] = numpy.linspace(0.0, self._opts['T'], 1000)

    ## Function set trajectory into self._out[] dictionary
    # @param self the object pointer
    # @param param a dictionary of the parameters
    def setOutTrajectory(self,param):
        opts_integrator = {}
        opts_integrator["grid"] = self._out['tgrid']

        system = {'t':self._system._t,
                  'x':self._system._q,
                  'p':self._system._control._lambda,
                  'ode':self._system.dq(self._system._q,
                                        self._system._u
                                        )
                  }
        Im = integrator("Im", "cvodes", system,opts_integrator)


        q =  horzcat(self._opts['q0'],Im(x0=self._opts['q0'],p=param['lambda'])['xf'])
        self._out['q'] = q[:self._system._dim['n'],:]


    def setOutJointsPositions(self,param):
        # x_pom = vertcat(float(param['x'][0])%2*pi,
        #                 param['x'][1],
        #                 float(param['x'][2])%2*pi)


        if len(self._opts['x0']) == 0:
            self._out['x'] = [0]
            dx = [0]
            for i in range(1,self._out['tgrid'].shape[0]):
                self._out['x'] = horzcat(self._out['x'],0)
        else:
            self._out['x'] = self._opts['x0']
            dx = (param['x']-self._opts['x0'])/len(self._out['tgrid'])
            for i in range(1,self._out['tgrid'].shape[0]):
                self._out['x'] = horzcat(self._out['x'],self._opts['x0']+i*dx)

    ## Function set y = k(q,x) into self._out[] dictionary
    # @param self the object pointer
    # @param param a dictionary of the parameters
    def setOutY(self,param):
        self._out['y'] = self._system.y(self._out['q'][:,0],
                                        self._out['x'][:,0]
                                                    )

        for i in range(1,len(self._out['tgrid'])):
            self._out['y'] = horzcat(self._out['y'],
                                     self._system.y(self._out['q'][:,i],
                                                    self._out['x'][:,i]
                                                    )
                                     )

    ## Function set control values at each time in time vector into self._out[] dictionary
    # @param self the object pointer
    # @param param a dictionary of the parameters
    def setOutControl(self,param):
        u = []
        for i in range( numpy.shape(self._out['tgrid'])[0]):
            u = horzcat(u , self._system._control.u( self._out['tgrid'][i],param['lambda']))

        self._out['u'] = u

    ## Function set error into self._out[] dictionary
    # @param self the object pointer
    # @param param a dictionary of the parameters
    def setOutError(self,param):
        self._out['e'] = param['error_vect']

    ## Function set normalized error into self._out[] dictionary
    # @param self the object pointer
    # @param param a dictionary of the parameters
    def setOutErrorNorm(self,param):
        en = []
        for i in range(param['error_vect'][0].size2()):
            en = numpy.hstack((en ,numpy.linalg.norm(self._out['e'][0][:,i])))

        self._out['en'] = [en]

        for j in range(1,len(param['error_vect'])):
            en = []
            for i in range(self._out['e'][j].size2()):
                en = numpy.hstack((en ,numpy.linalg.norm(self._out['e'][j][:,i])))

            self._out['en'].append(en)