from sys import path
path.append(r"../../casadi-py27-np1.9.1-v3.0.0")
from casadi import *


## Module Control
# The class provide the interface to build a control for a system.
# A control is define as
# \f[
# u(t) = P(t) \lambda, \quad u \in \mathbb{R}^m,
# \f]
# where:
# - \f$P(t)\f$ is a block diagonal matrix whose blocks comprise a number of basic
# orthogonal functions in the Hilbert control space \f$\mathbb{L}^2_m[0,T] \f$,
# - \f$ \lambda \in \mathbb{R}^s \f$ is a collection of control parameters.
class Control:
    ## The initialization function
    # @param self the object pointer
    # @param opts a python dictionary with options like time horizon \f$T\f$
    def __init__(self,opts):
        ## a dictionary of options
        #
        # Necessary options are:
        # - T -- time horizon
        # - lambda -- is a list of following elements [length of lambda coefficients, from which point of the sequence start]. Each element refer to each control for example the first element refer to the first control and so on.
        self._opts = opts
        ## a dictionary of dimensions
        #
        # Necessary dimensions are:
        # - s -- dimension of lambda vector, \f$\lambda \in \mathbb{R}^s\f$
        # - m -- dimension of control, \f$u \in \mathbb{R}^m\f$
        self._dim = {}
        self._dim['m'] = len(self._opts['lambda'])
        self._dim['s'] = 0
        for i in self._opts['lambda']:
            self._dim['s'] = self._dim['s'] + i[0]
        ## time horizon
        self._T = opts['T']
        ## begin time
        self._Tb = 0
        ## vector of CasADi symbolic \f$\lambda\f$ coefficients
        self._lambda = SX.sym("lambda",self._dim['s'])

    ## Definition of \f$P(t)\f$ matrix
    # @param self the object pointer
    # @param t the time (symbolic or numerical)
    # @retval P \f$P(t)\f$ matrix
    def P(self,t):
        raise "Implement your own P(t)"

    ## The function gives control
    # @param self the object pointer
    # @param t the time (symbolic or numerical)
    # @param lambdas_coefficients vector of \f$ \lambda \f$ control parameters (symbolic or numerical)
    # @return \f$ P(t)\lambda \f$
    def u(self,t,lambdas_coefficients):
        return mtimes(self.P(t),lambdas_coefficients)

    ## Set time horizon \f$ T \f$
    # @param self the object pointer
    # @param T the time horizon
    def setT(self,T):
        self._T = T

    ## Give time horizon \f$ T \f$
    # @param self the object pointer
    # @retval self._T actual time horizon
    def getT(self):
        return self._T

    ## Set the begin time \f$ T_b \f$
    # @param self the object pointer
    # @param Tb the begin time
    def setTb(self,Tb):
        self._Tb = Tb

    ## Give the begin time \f$ T_b \f$
    # @param self the object pointer
    # @retval self._Tb actual begin time
    def getTb(self):
        return self._Tb


