from sys import path
path.append(r"../casadi-py27-np1.9.1-v3.0.0")
from casadi import *

from ECSA.API.ECSAAlgorithm import ECSAAlgorithm

## The class model MultiShooting group of algorithms
class ECSAMultiShooting(ECSAAlgorithm):
    ## The initialization method
    #
    # @param self the object pointer
    # @param system the object of the class System or System_base
    # @param opts a dictionary of algorithm options
    def __init__(self,system,opts):
        ECSAAlgorithm.__init__(self, system, opts)

    ## Function check initial data
    #
    # @retval 1 everything is OK
    # @retval 0 something wrong
    def checkData(self):
        if self._system._dim['n'] != len(self._opts['q0']):
            print "The dimension of q0 is different that the dimension n"
            self._out['fault'] = 1
            return 0
        elif self._system._dim['p'] != len(self._opts['x0']):
            print "The dimension of x0 is different that the dimension p"
            self._out['fault'] = 1
            return 0
        elif self._system._dim['m'] != self._system._control._dim['m']:
            print "The dimension of controls is different that the dimension m"
            self._out['fault'] = 1
            return 0
        else:
            self._out['fault'] = 0
            return 1

    ## The function define jacobian and Error
    # @param self the object pointer
    # @param opts a dictionary of options for the algorithm
    # @param param a dictionary of the parameters
    # @retval param a dictionary of parameter, e.g Jacobian and Error
    def getJacobianAndError(self,param,opts):
        raise "Define Jacoian and Error"

    ## The function define help variable
    #  @param self the object pointer
    # @param opts a dictionary of options for the algorithm
    # @param param a dictionary of the parameters
    # @retval param a dictionary of new parameters
    def getHelpVariable(self,param,opts):
        raise "Define help variable"

    ## The function define gamma parameter
    # @param self the object pointer
    # @param opts a dictionary of options for the algorithm
    # @param param a dictionary of the parameters
    # @retval [gamma1, ...] a list
    def getGamma(self,param,opts):
        raise "Implement your definition of gamma"

    ## The function perform newton step
    # @param self the object pointer
    # @param opts a dictionary of options for the algorithm
    # @param param a dictionary of the parameters
    # @retval lambda,s new lambda vector and new of 's'
    def getNewtonStep(self,param,opts):
        raise "Implement your definition of new lambda"

    ## Function provide while loop for the ECSA
    #
    # @param self the object pointer
    # @param opts a dictionary of options for the algorithm
    # @param param a dictionary of the parameters
    # @retval param a dictionary of the final parameters
    def mainLoop(self,param,opts):
        while param['step'] < opts['k_max'] and numpy.linalg.norm(param['G'].flatten()) > opts['TOL']:
            param = self.getJacobianAndError(param,opts)
            param = self.getHelpVariable(param,opts)
            param['dlambda'],param['ds'] = self.getNewtonStepLength(param,opts)
            param['gamma'] = self.getGamma(param,opts)
            param['lambda'],param['s'] = self.getNewtonStep(param,opts)
            param['step'] = param['step'] + 1

            self.updateErrorVector(param)
            self.visualizationOneStep(param,opts)

        return param

    ## Function put actual error and put it into param['error_vect']
    # @param self the object pointer
    # @param param a dictionary of the parameters
    def updateErrorVector(self,param):
        param['k_vect'] = vertcat(param['k_vect'],
                                  float(norm_2(param['G'].flatten())))

    ## Function visualize the desired parameters at each step
    #
    # @param self the object pointer
    # @param opts a dictionary of options for the algorithm
    # @param param a dictionary of the parameters
    def visualizationOneStep(self,param,opts):
            print "step: %d " % param['step']
            print "%1s error: %.5f" %("",numpy.linalg.norm(param['G'].flatten()))
            # print "%1s det: %.5f" %("",numpy.linalg.det(mtimes(param['dHds'],param['dHds'].T)))
            # print "%1s det: %e" %("",numpy.linalg.det(mtimes(param['Jp'].T,param['Jp'])))
            # print "%1s rank of Jacobi matrix: %d" %("",numpy.linalg.matrix_rank(param['Jp']))
            # print "%1s condition number: %.2f" %("",numpy.linalg.cond(param['Jp']))

            if opts['viz'][0] == True:
                self.setOutput(param)
                ECSA.API.Visualization.showPath(self._out,
                                       self._opts,
                                       isBlocked=opts['viz'][1],
                                       pauseTime=opts['viz'][2]
                                       )

    ## Function set number of algorithm steps into self._out[] dictionary
    # @param self the object pointer
    # @param param a dictionary of the parameters
    def setOutStep(self,param):
        self._out['step'] = param['step']

    ## Function set lambda vector into self._out[] dictionary
    # @param self the object pointer
    # @param param a dictionary of the parameters
    def setOutLambda(self,param):
        self._out['lambda'] = param['lambda']
        self._out['s'] = param['s']

    ## Function set time vector into self._out[] dictionary
    # @param self the object pointer
    # @param param a dictionary of the parameters
    def setOutTime(self,param):
        self._out['tgrid'] = numpy.linspace(0.0, self._opts['T'], 1000)

    ## Function set trajectory into self._out[] dictionary
    # @param self the object pointer
    # @param param a dictionary of the parameters
    def setOutTrajectory(self,param):
        # opts_integrator = {}
        # opts_integrator["grid"] = self._out['t']
        #
        system = {'t':self._system._t,
                  'x':self._system._q,
                  'p':self._system._control._lambda,
                  'ode':self._system.dq(self._system._q,
                                        self._system._u
                                        )
                  }
        q = []
        tgrid = []
        for m in range(param['N']):
            tm = numpy.linspace(param['Tn']*(m), param['Tn']*(m+1), 1000/param['N'])
            if m == 0:
                q0 = self._opts['q0']
            else:
                q0 = param['s'][m-1]
            lambda0 = param['lambda'][m]

            integrator_opts = {}
            integrator_opts["grid"] = tm

            ## integrator
            I = integrator("I", "cvodes", system, integrator_opts)
            sol =  I(x0=q0,p=lambda0)
            q_new = numpy.array(sol["xf"])
            q_new = horzcat(q0,q_new)
            q = horzcat(q,q_new)
            # print "   ",tm.shape,q.shape
            tgrid = numpy.append(tgrid,tm)

        # print tgrid.shape,q.shape

        self._out['q'] = q
        self._out['N'] = param['N']

    def setOutJointsPositions(self,param):
        pass

    ## Function set y = k(q,x) into self._out[] dictionary
    # @param self the object pointer
    # @param param a dictionary of the parameters
    def setOutY(self,param):
        self._out['y'] = self._out['q']

    ## Function set control values at each time in time vector into self._out[] dictionary
    # @param self the object pointer
    # @param param a dictionary of the parameters
    def setOutControl(self,param):
        u = []
        for m in range(param['N']):
            tm = numpy.linspace(param['Tn']*(m), param['Tn']*(m+1), 1000/param['N'])
            for i in range(numpy.shape(tm)[0]):
                u = horzcat(u,self._system._control.u(tm[i],param['lambda'][m]))
        self._out['u'] = u

    ## Function set error into self._out[] dictionary
    # @param self the object pointer
    # @param param a dictionary of the parameters
    def setOutError(self,param):
        self._out['e'] = param['k_vect']

    ## Function set normalized error into self._out[] dictionary
    # @param self the object pointer
    # @param param a dictionary of the parameters
    def setOutErrorNorm(self,param):
        en = []
        for i in range(self._out['e'].size1()):
            en = numpy.hstack((en ,numpy.linalg.norm(self._out['e'][i])))

        self._out['en'] = [en]