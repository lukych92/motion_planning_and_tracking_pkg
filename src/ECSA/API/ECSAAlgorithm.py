from sys import path
path.append(r"../casadi-py27-np1.9.1-v3.0.0")
from casadi import *


## The class model general ECSA algorithm
class ECSAAlgorithm:
    ## The initialization method
    #
    # @param self the object pointer
    # @param system the object of the class System or System_base
    # @param opts a dictionary of algorithm options
    def __init__(self,system,opts):
        ## the object of System or System_base class
        self._system = system

        ## a dictionary of options parameters
        #
        # Necessary options parameters are:
        # - T -- time horizon
        # - gamma0 -- gamma parameter
        # - TOL -- tolerance error
        # - k_max -- maximum number of algorithms steps
        # - q0 -- list of initial state
        # - x0 -- list of initial joint position
        # - yd -- list of desired positions
        # - lambda0 -- vector of initial lambda coefficients
        # - viz -- a list of parameters to visualization [show picture at each step, block showing the new picture, time delay between pictures]
        self._opts = opts

        ## a dictionary of output parameters
        #
        # The dictionary contains:
        # - fault -- if 0 is OK, if 1 something wrong during algorithm procedure
        # - step -- number of algorithms steps
        # - lambda -- vector of lambda coefficients
        # - tgrid -- vector of discretized time
        # - q -- trajectory vector
        # - x -- joints positions vector
        # - y -- vector of output function \f$ y = k(q,x) \f$
        # - u -- vector of controls values
        # - e -- vector of error at each step
        # - en -- vector of normalized error at each step
        self._out = {}

    ## Function check initial data
    #
    # @retval 1 everything is OK
    # @retval 0 something wrong
    def checkData(self):
        raise "Define procedure of checking data"

    ## Function start all the algorithm
    #
    # @param self the object pointer
    # @retval self._out a dictionary of final parameters
    def run(self):
        if self.checkData():
            param = self.init(self._opts)
            param = self.mainLoop(param,self._opts)

            self.setOutput(param)
        return self._out

    ## Function for initialize of the variables
    #
    # @param self the object pointer
    # @param opts a dictionary of options for the algorithm
    # @retval param a dictionary of the final parameters
    def init(self,opts):
        raise "Define the init function"

    ## Function provide while loop for the ECSA
    #
    # @param self the object pointer
    # @param opts a dictionary of options for the algorithm
    # @param param a dictionary of the parameters
    # @retval param a dictionary of the final parameters
    def mainLoop(self,param,opts):
        raise "Define the main loop"

    ## Function visualize the desired parameters at each step
    #
    # @param self the object pointer
    # @param opts a dictionary of options for the algorithm
    # @param param a dictionary of the parameters
    def visualizationOneStep(self,param,opts):
        raise "Implement your own vizualization function"

    ## Function set output parameters
    #
    # @param self the object pointer
    # @param param a dictionary of the parameters
    def setOutput(self,param):
        self.setOutStep(param)
        self.setOutLambda(param)
        self.setOutTime(param)
        self.setOutTrajectory(param)
        self.setOutJointsPositions(param)
        self.setOutY(param)
        self.setOutControl(param)
        self.setOutError(param)
        self.setOutErrorNorm(param)

    ## Function set output steo parameter
    #
    # @param self the object pointer
    # @param param a dictionary of the parameters
    def setOutStep(self,param):
        raise "Define your output (last) step"

    ## Function set output lambda vector
    #
    # @param self the object pointer
    # @param param a dictionary of the parameters
    def setOutLambda(self,param):
        raise "Define your output lambda"

    ## Function set output time vector
    #
    # @param self the object pointer
    # @param param a dictionary of the parameters
    def setOutTime(self,param):
        raise "Define your output time vectror"

    ## Function set output trajectory
    #
    # @param self the object pointer
    # @param param a dictionary of the parameters
    def setOutTrajectory(self,param):
        raise "Define your output trajectory"

    def setOutJointsPositions(self,param):
        raise "Define your output joints positions"

    ## Function set output y
    #
    # @param self the object pointer
    # @param param a dictionary of the parameters
    def setOutY(self,param):
        raise "Define your output y"

    ## Function set output control
    #
    # @param self the object pointer
    # @param param a dictionary of the parameters
    def setOutControl(self,param):
        raise "Define your output control"

    ## Function set output error
    #
    # @param self the object pointer
    # @param param a dictionary of the parameters
    def setOutError(self,param):
        raise "Define your output error"

    ## Function set output normalized error
    #
    # @param self the object pointer
    # @param param a dictionary of the parameters
    def setOutErrorNorm(self,param):
        raise "Define your output norm error"
