from sys import path
path.append(r"../casadi-py27-np1.9.1-v3.0.0")
from casadi import *

import pylab
from itertools import cycle

def showPath(out,opts,pam=[0,1],isBlocked=True,pauseTime=0):
    if out['fault']:
        return 1
    sol = out['y']
    q = out['q']

    pylab.plot(sol[pam[0],:].T,sol[pam[1],:].T,'k',label='y')
    pylab.plot(q[pam[0],:].T,q[pam[1],:].T,'k--',label = 'q')
    pylab.plot(opts['q0'][pam[0]],opts['q0'][pam[1]],'ko', label="Initial position")

    if 'N' in out.keys():
        N = out['N']
        pylab.plot(opts['yd'][N-1][pam[0]],opts['yd'][N-1][pam[1]],'kx',label="Final position")
    else:
        N = 1
        pylab.plot(opts['yd'][pam[0]],opts['yd'][pam[1]],'kx',label="Final position")

    # pylab.figure(1)

    for i in range(N-1):
        pylab.plot(opts['yd'][i][pam[0]],opts['yd'][i][pam[1]],'ks')
    pylab.title('Path')
    pylab.legend(loc=0, numpoints=1)
    pylab.xlabel('q%d, y%d' %(pam[0]+1,pam[0]+1))
    pylab.ylabel('q%d, y%d' %(pam[1]+1,pam[1]+1))
    pylab.show(isBlocked)
    pylab.pause(pauseTime)
    pylab.clf()

def showTrajectory(out,bounds={}):
    if out['fault']:
        return 1
    tgrid = out['tgrid']
    sol = out['q']

    for i in range(sol.size1()):
        num = (sol.size1())*100+10+i+1
        pylab.subplot(num)
        pylab.plot(tgrid[:],sol[i,:].T,'k')
        whichState = 'q'+str(i)
        if whichState in bounds:
            pylab.plot(tgrid,[bounds[whichState][0] for x in range(numpy.shape(tgrid)[0])],'k--')
            pylab.plot(tgrid,[bounds[whichState][1] for x in range(numpy.shape(tgrid)[0])],'k--')
        pylab.ylabel('q'+str(i+1))
        if i == 0:
            pylab.title('Trajectories')


    pylab.xlabel('time')
    pylab.show()

def showControl(out,bounds={}):
    if out['fault']:
        return 1
    tgrid = out['tgrid']
    u = out['u']

    if 'N' in out.keys():
        N = out['N']
    else:
        N = 1

    steps = 1000
    for i in range(u.size1()):
        pylab.figure(i+1)
        name = "u"+str(i)

        for m in range(N):
            pylab.plot(tgrid[m*steps/N:(m+1)*steps/N],
                       u[i,m*steps/N:(m+1)*steps/N].T,
                       'k',
                       label=("u%d" %(i+1))
                       )
            pylab.title('Control')
            pylab.xlabel('time')
            pylab.ylabel("u%d" %(i+1))

        if name in bounds:
            down_bound = bounds[name][0]
            upper_bound = bounds[name][1]

            pylab.plot(tgrid,[down_bound for x in range(numpy.shape(tgrid)[0])],'k--')
            pylab.plot(tgrid,[upper_bound for x in range(numpy.shape(tgrid)[0])],'k--')
            pylab.ylim(min(down_bound,numpy.min(u[i,:]))-1, max(upper_bound,numpy.max(u[i,:]))+1)

        pylab.show()

def showError(out):
    if out['fault']:
        return 1
    x = [x+1 for x in range(out['step'])]
    y = out['en']

    lines = ["k-","k--","k-.","k:"]
    linecycler = cycle(lines)
    pylab.figure(1)
    for i in range(len(y)):
        pylab.plot(x,y[i], next(linecycler),label='Error '+str(i+1))
    pylab.legend(loc=0,)
    pylab.title('Convergence of error')
    pylab.ylabel('error')
    pylab.xlabel('iteration')
    pylab.xlim(1, out['step'])
    pylab.show()


