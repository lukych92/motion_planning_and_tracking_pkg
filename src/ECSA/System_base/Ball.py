from sys import path
path.append(r"../casadi-py27-np1.9.1-v3.0.0")
from casadi import *

from ECSA.API import System_base

## Class is a definition of ball
#
# Mathematical model of the ball:
# \f[ \begin{cases}
# \dot{q_1} = u_1 sin(q_4) sin(q_5) + u_2 cos(q_5) \\
# \dot{q_2} = -u_1 sin(q_4) cos(q_5) + u_2 sin(q_5) \\
# \dot{q_3} = u_1 \\
# \dot{q_4} = u_2 \\
# \dot{q_5} = -u_1 cos(q_4) \\
# y = k(q,x) = (q_1,q_2,q_5)
# \end{cases}
# \f]
#
# \image latex ball.eps "Ball" width=12cm
class Ball(System_base):
    ## The initialization function
    # @param self the object pointer
    # @param dim a python dictionary of system dimensions
    # @param control an object of class Control
    def __init__(self,control,opts={}):
        ## dimensions of the ball
        self._dim = {
            'n':5,#dim q
            'p':0,#dim x
            'r':3,#dim y
            'm':2,#dim u
        }
        System_base.__init__(self,self._dim,control)

    ## Definition of state evolution
    # @param self the object pointer
    # @param q state variables
    # @param u control
    def dq(self,q,u):
        dq = vertcat(u[0]*sin(q[3])*sin(q[4])+u[1]*cos(q[4]),
                     -u[0]*sin(q[3])*cos(q[4])+u[1]*sin(q[4]),
                     u[0],
                     u[1],
                     -u[0]*cos(q[3])
                     )
        return dq


    ## Definition of output
    # @param self the object pointer
    # @param q state variables
    # @param x joint position
    def y(self,q,x):
        y = vertcat(q[0],
                    q[1],
                    q[4]
                    )
        return y