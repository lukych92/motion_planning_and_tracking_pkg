from sys import path
path.append(r"../../casadi-py27-np1.9.1-v3.0.0")
from casadi import *


from ECSA.Control import Jacobi

## Legendre polynomial control base
#
# Legendre is a Jacobi polynomial with \f$ \alpha = \beta = 0 \f$
class Legendre(Jacobi):
    def __init__(self,opts):
        alpha = 0
        beta = 0
        Jacobi.__init__(self,alpha,beta,opts)