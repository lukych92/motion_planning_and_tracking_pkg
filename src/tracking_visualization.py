#!/usr/bin/env python

#/*****************************************************************************
# *                                                                           *
# *   TrackingTrajectoryVisualization.py                                                           *
# *                                                                           *
# *   ROS                                                                     *
# *                                                                           *
# *   Copyright (C) 2015 by Lukasz Chojnacki                                  *
# *   lukasz,chojnacki@pwr.edu.pl                                             *
# *                                                                           *
# *   This program is free software; you can redistribute it and/or modify    *
# *   it under the terms of the GNU General Public License as published by    *
# *   the Free Software Foundation; either version 2 of the License, or       *
# *   (at your option) any later version.                                     *
# *                                                                           *
# *   This program is distributed in the hope that it will be useful,         *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
# *   GNU General Public License for more details.                            *
# *                                                                           *
# *   You should have received a copy of the GNU General Public License       *
# *   along with this program; if not, write to the                           *
# *   Free Software Foundation, Inc.,                                         *
# *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
# *                                                                           *
# *****************************************************************************/



import rospy
import time

from pylab import figure,plot,show,close
from robrex.msg import ready, traj

def Trajectory(data):
    # global Q1, Q2

    q1 = data.q1
    q2 = data.q2
    q3 = data.q3

    # Q1 = concatenate((Q1, q1))
    # Q2 = concatenate((Q2, q2))


    ## start tracking the trajectory
    figure()
    # ax1 = f1.add_subplot(111)
    plot(q1,q2)
    show(block=True)
    time.sleep(2)
    close()
    ## stop tracking the trajectory

    msg = ready()
    msg.ready = True
    pubReady.publish(msg)


def node():
    global pubReady
    # global Q1, Q2
    #
    # # Init vars
    # Q1 = array([])
    # Q2 = array([])

    # Init ROS Node /LiftedNewtonAlgorithm
    rospy.init_node('tracking_visualization')

    # Publisher
    pubReady = rospy.Publisher("ready", ready, queue_size=100)

    # Subscriber
    rospy.Subscriber("traj", traj, Trajectory)

    rospy.spin()


if __name__ == '__main__':
    try:
        node()
    except rospy.ROSInterruptException:
        pass
